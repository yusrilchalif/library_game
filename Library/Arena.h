#pragma once
class Arena
{
public:
	Arena();
	void SetValue(int, int, char);
	char GetValue(int, int);
	void printMap();

	~Arena();

private:
	char Map[16][16] = {
		"{=============}",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"|             |",
		"[=============]",
	};

};


