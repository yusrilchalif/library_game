#include "GameRules.h"
//#include "Player.h"
#include <conio.h>

// Constructor overload dari class GameRules dengan 2 parameter, yaitu variabel dari class Player dan variabel dari class Map.

GameRules::GameRules(Player *player, Map *map)
{
	this->player = player;
	this->map = map;
	//this->arena = arena;
}

// Fungsi yang menjalankan game.
void GameRules::play()
{
	map->draw();
	player->draw();
	//arena->printMap();
}

// Fungsi yang mengatur pergerakan dari player.
void GameRules::control()
{
	while (true) 
	{
		// Pengecekan inputan dari keyboard.
		if (_kbhit()) 
		{
			//gotoxy();
			// Mengambil data inputan dari keyboard.
			switch (_getch())
			{
			case 'w': // Apabila user menekan huruf w
				if (player->getY() > 1)
				{
					player->erase();
					player->moveUp();
					player->draw();
				}
				break;

			case 'a': // Apabila user menekan huruf a
				if (player->getX() > 1)
				{
					player->erase();
					player->moveLeft();
					player->draw();
				}
				break;

			case 's': // Apabila user menekan huruf s
				if (player->getY() < 13)
				{
					player->erase();
					player->moveDown();
					player->draw();
				}
				break;

			case 'd': // Apabila user menekan huruf d
				if (player->getX() < 13)
				{
					player->erase();
					player->moveRight();
					player->draw();
				}
				break;

			default:
				break;
			}
		}
	}
}

// Fungsi yang digunakan untuk memanggil obstacle.
void GameRules::spawnObstacle(std::string obs, int posX, int posY)
{
	/*auto instanceOne = ObstacleFactory::DrawObstacle("Plus");
	auto instanceTwo = ObstacleFactory::DrawObstacle("Minus");

	instanceOne->draw();
	instanceTwo->draw();*/

	of->DrawObstacle(obs);
}

// Fungsi yang digunakan untuk mengecek apakah permainan telah selesai (kalah) atau belum.
bool GameRules::checkLose()
{
	return false;
}
