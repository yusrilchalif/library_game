#include "Player.h"
#include "Map.h"
#include "GameRules.h"
#include "Arena.h"
#include "Obstacle.h"
#include "ObstacleFactory.h"

using namespace std;

int main()
{
	Map map;
	map.draw();

	Player player;
	player.draw();

	system("Pause");
}
