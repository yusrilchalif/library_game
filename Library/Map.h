#pragma once
#include "Player.h"

// Class Map merupakan class yang berisi segalanya tentang map.
class Map
{
private:
	//int x; // Variabel yang digunakan untuk menentukan jumlah baris.
	//int y; // Variabel yang digunakan untuk menentukan jumlah kolom.
	char map[15][15] = { // Variabel yang digunakan untuk menggambarkan bentuk dari map.
		{'{', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '}'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'+', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '+'},
		{'^', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '^'}
	};

public:
	void draw(); // Fungsi yang digunakan untuk menggambar map.	
	Map();
	void SetValue(int i, int j, int a);
	//void SetValue(int i, int j, char a);
	char GetValue(int i, int j);
	bool isFull();
	void ResetMap();

	char checkDiagonal();
	char checkVertical();
	char checkHorizontal();
	bool isAvailable(int i, int j);
};

