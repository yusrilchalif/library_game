#pragma once
#include <iostream>

// Class Player merupakan class yang berisi segalanya tentang player (pemain).
class Player
{
private:
	int x; // Variabel yang digunakan untuk menentukan posisi x character.
	int y; // Variabel yang digunakan untuk menentukan posisi y character.
	char character = 'O'; // Variabel yang digunakan untuk menggambarkan bentuk character.

public:
	Player();  // Constructor dari class Player.
	Player(int x, int y); // Constructor overload dari class Player yang memiliki 2 parameter untuk posisi x dan y character.
	void setPosition(int x, int y); // Fungsi yang digunakan untuk mengatur posisi dari character berdasarkan parameter x dan y.
	int getX(); // Fungsi untuk mendapatkan posisi x character.
	int getY(); // Fungsi untuk mendapatkan posisi y character.
	void moveUp(); // Fungsi yang berisi pergerakan character ke atas.
	void moveDown(); // Fungsi yang berisi pergerakan character ke bawah.
	void moveRight(); // Fungsi yang berisi pergerakan character ke kanan.
	void moveLeft(); // Fungsi yang berisi pergerakan character ke kiri.
	void draw(); // Fungsi untuk menggambarkan posisi character dengan posisi x dan y yang baru.
	void erase(); // Fungsi untuk menghapuskan  character dari posisi x dan y yang lama.
};

